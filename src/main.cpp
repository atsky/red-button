#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

const char* ssid = "MOWMOW";
const char* password =  "melpastable";

#define OLED_RESET -1
Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &Wire, OLED_RESET);

#define BUTTON_PIN 5

const char* numbers[] = {"0", "1", "2", "3", "4", "5"};

enum StateEnum {
  NOT_READY,
  READY,
  DEPLOING,
  DO_DEPLOY,
  STOPPED,
  DEPLOYED,
  FAILED
};

StateEnum state;
int countdown = -1;

void clear() {
  display.clearDisplay();
  display.setTextColor(WHITE);
}

void drawInCenter(const char* text) {
  int16_t x1, y1; 
  uint16_t w, h;
  display.getTextBounds(text, 0, 0, &x1, &y1, &w, &h);

  int16_t x0 = (128 - w) / 2; 
  int16_t y0 = (32 - h) / 2; 

  display.setCursor(x0, y0);
  display.setTextWrap(false);
  display.print(text); 
}

void drawInCenter2(const char* text1, const char* text2) {
  int16_t x1, y1; 
  uint16_t w1, h1;
  display.getTextBounds(text1, 0, 0, &x1, &y1, &w1, &h1);

  uint16_t w2, h2;
  display.getTextBounds(text2, 0, 0, &x1, &y1, &w2, &h2);

  display.setTextWrap(false);

  int16_t x0 = (128 - w1) / 2; 
  int16_t y0 = (32 - h1 - h2) / 2; 

  display.setCursor(x0, y0);
  display.print(text1); 

  x0 = (128 - w2) / 2; 
  y0 += h1; 

  display.setCursor(x0, y0);
  display.print(text2); 
}

void showConnectingToWIFI() {
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(2);
  drawInCenter2("Conneting", "to WIFI");
  display.display();
}

int dx = 0;
const char* lastText = NULL;

bool showMovingText(const char* text) {
  clear();
  display.setTextSize(3);

  if (lastText != text) {
    lastText = text;
    dx = 128;
  }
  int16_t x1, y1; 
  uint16_t w, h;
  display.getTextBounds(text, 0, 0, &x1, &y1, &w, &h);

  int16_t y0 = (32 - h) / 2; 

  display.setCursor(dx, y0);
  display.setTextWrap(false);

  display.print(text); 

  display.display();

  dx -= 4;

  if (dx <= -w) {
    dx = 128;
    return true;
  }
  return false;
}

void setup()
{
  WiFi.begin(ssid, password);
  Wire.begin();
  pinMode(BUTTON_PIN, INPUT);  
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

  int steps = 0;

  while (WiFi.status() != WL_CONNECTED && steps < 20) {
    showConnectingToWIFI();
    delay(50);
    steps++;
  }
  state = NOT_READY;
  dx = 128;
}

void showReady(){
  clear();
  display.setTextSize(3);
  drawInCenter("Ready!");    
  display.display();
}

void showDeployed() {
  clear();
  display.setTextSize(2);
  drawInCenter("Deployed");    
  display.display();
}

void showFailed() {
  clear();
  display.setTextSize(2);
  drawInCenter("Failed");    
  display.display();
}

void showWifiError() {
  clear();
  display.setTextSize(2);
  drawInCenter2("WIFI not", "connected");
  display.display();
}

void showNotReadyError() {
  showMovingText("Not ready! Release button");
}

bool buttonPushed() {
  return digitalRead(BUTTON_PIN);
}


void fadeOut() {
  for (int dim=150; dim>=0; dim-=10) {
    display.ssd1306_command(0x81);
    display.ssd1306_command(dim); //max 157
    delay(40);
  }
  
  
  for (int dim2=34; dim2>=0; dim2-=17) {
    display.ssd1306_command(0xD9);
    display.ssd1306_command(dim2);  //max 34
    delay(80);
  }

  clear();
  display.ssd1306_command(0xD9);
  display.ssd1306_command(34);  //max 34 

  display.ssd1306_command(0x81);
  display.ssd1306_command(157); //max 157
}

void doDeploy() {
  HTTPClient http;  //Declare an object of class HTTPClient
 
  http.begin("http://www.google.com");  //Specify request destination
  int httpCode = http.GET();                                  //Send the request
 
  if (httpCode > 0) { //Check the returning code
    state = DEPLOYED;
  } else {
    state = FAILED;
  }
 
  http.end();
}

void loop()
{
  if (WiFi.status() != WL_CONNECTED) {
    showWifiError();
    display.display();
    state = NOT_READY;
    return;
  }

  switch (state) {
  case NOT_READY:
    if (buttonPushed()) {
      showNotReadyError();
    } else {
      state = READY; 
    }
    break;
  case READY:
    if (buttonPushed()) {
      countdown = -1;
      state = DEPLOING;
    } else {
      showReady();
    }
    break;

  case DEPLOING:
    if (buttonPushed()) {
      if (countdown == -1) {
        if (showMovingText("Deploy sequence initated!")) {
          countdown = 5;
        }
      } else {
        clear();
        display.setTextSize(4);
        drawInCenter(numbers[countdown]);    
        display.display();
        fadeOut();
        countdown--;
        if (countdown == -1) {
          state = DO_DEPLOY;
        }
      }
    } else {
      state = STOPPED;
    }
    break;
  case STOPPED:
    if (showMovingText("Deploy aborted")) {
      state = READY;
    }
    break;
  case DO_DEPLOY:
    clear();
    display.setTextSize(2);
    drawInCenter("Deploing");    
    display.display();
    delay(200);
    
    doDeploy();
    break;
  case DEPLOYED:
    if (buttonPushed()) {
      showDeployed();
    } else {
      state = READY;
    }
    break;
  case FAILED:
    if (buttonPushed()) {
      showFailed();
    } else {
      state = READY;
    }
    break;
  default:
    clear();
    display.setTextSize(2);
    drawInCenter("TODO!!!");
    display.display();
    break;
  }

  /*
  if (digitalRead(BUTTON_PIN)) {
    // Button pushed
    showStart();
    dx -= 2;
    if (dx <= -100) {
      dx = 40;
    }
  } else {
    showMain();
  }
  */

  delay(20);
}